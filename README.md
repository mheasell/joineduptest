# JoinedUp Test Solution

## Pre-requisites

### MySQL

The project expects a MySQL server to be reachable on localhost port 3306.
It connects to database 'joineduptest' with username 'joineduptest'
and an empty password.
This database should contain the data from the SQL dump.

### Perl and Perl Modules

This project runs on Linux. You will need perl and Carton.
I recommend using perlbrew (https://perlbrew.pl/)
to install a recent perl locally,
install cpanm with `perlbrew install-cpanm`,
then do `cpanm Carton` to install Carton.
The project was developed on perl 5.28.0,
but in practice older perls should work too.

On Ubuntu, you will need to install `libmysqlclient-dev`
before installing the perl modules, as it is required
by the MySQL database adapter during compilation:

    sudo apt-get install -y libmysqlclient-dev

Now use carton to install the required dependencies:

    cd /path/to/project
    carton install

The project is written using the Mojolicious web framework, plus some supporting libraries
for database access, date/time handling and testing.


## How to run

Run the Mojolicious development server from the project root directory using carton:

    carton exec morbo ./app.pl

The site should now be available on http://localhost:3000/

To run the unit tests, do:

    carton exec ./t/Lunchtron.t
