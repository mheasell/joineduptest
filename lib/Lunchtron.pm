package Lunchtron;

use strict;
use warnings;

use Exporter qw(import);

our @EXPORT_OK = qw(is_subset find_max_coverage create_meal_user_mapping);

# Returns true if the keys of hash a
# are a subset of the keys of hash b.
# i.e. b contains at least all the keys a does.
sub is_subset {
    my ($a, $b) = @_;
    for my $k (keys(%$a)) {
        if (!exists($b->{$k})) {
            return 0;
        }
    }

    return 1;
}

# From the given sets, chooses k sets that have
# approximately the maximum coverage of unique elements.
#
# This function finds an approximate solution
# with a straightforward greedy algorithm.
# An optimal solution to this problem is NP-hard.
sub find_max_coverage {
    my ($sets, $k) = @_;

    my %chosen_sets;
    my %covered_elements;

    while (scalar(keys(%$sets)) > 0 && scalar(keys(%chosen_sets)) < $k) {
        # find the set that covers the most (new) elements
        my $best_score = -1;
        my $best_set_id;
        while (my ($id, $set) = each(%$sets)) {
            next if $chosen_sets{$id};
            my $score = 0;
            for my $k (keys(%$set)) {
                $score += 1 if not $covered_elements{$k};
            }
            if ($score > $best_score) {
                $best_score = $score;
                $best_set_id = $id;
            }
        }

        # add it to the chosen sets
        $chosen_sets{$best_set_id} = 1;
    }

    return \%chosen_sets;
}

sub create_meal_user_mapping {
    my ($meals, $users, $all_meal_categories, $all_user_food_preferences) = @_;

    my %meal_user_sets;
    for my $meal (@$meals) {
        for my $user (@$users) {
            if (is_subset($all_user_food_preferences->{$user->{id}}, $all_meal_categories->{$meal->{id}})) {
                $meal_user_sets{$meal->{id}}{$user->{id}} = 1;
            }
        }
    }

    return \%meal_user_sets;
}

1;
