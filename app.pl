#!/usr/bin/env perl

use warnings;
use strict;

use lib 'lib';

use Mojolicious::Lite;
use DBIx::Simple;
use DateTime;

use Lunchtron qw(is_subset find_max_coverage create_meal_user_mapping);

my $USER_ID = 1;

my $dbh = do {
    my $database = 'joineduptest';
    my $username = 'joineduptest';
    my $password = '';
    my $dsn = "DBI:mysql:database=$database;host=localhost;port=3306";
    DBIx::Simple->connect($dsn, $username, $password);
};

# Fetches info for the given user from the database.
# If no user exists with the given ID, returns undef.
sub get_user_info {
    my ($id) = @_;
    return $dbh->select('users', ['id', 'type', 'fullname'], { id => $id })->hash;
}

sub get_users {
    return $dbh->select('users', ['id', 'type', 'fullname'])->hashes;
}

sub get_food_categories {
    return $dbh->select('food_categories', ['id', 'name'])->hashes;
}

sub get_user_food_preferences {
    my ($user_id) = @_;
    return $dbh->select('user_food_preferences', ['user_id', 'food_category_id'], { user_id => $user_id })->hashes;
}

sub set_user_food_preferences {
    my ($user_id, $selected_categories) = @_;
    $dbh->begin_work;
    $dbh->delete('user_food_preferences', { user_id => $user_id });

    # this could be sped up using a prepared query or bulk insert
    for my $category_id (@$selected_categories) {
        $dbh->insert(
            'user_food_preferences',
            { user_id => $user_id, food_category_id => $category_id }
        );
    }

    $dbh->commit;
}

sub get_meals {
    return $dbh->select('meals', ['id', 'name'])->hashes;
}

sub get_all_meal_categories {
    my $rows = $dbh->select('meal_categories', ['meal_id', 'food_category_id'])->hashes;
    my %hash;
    for my $r (@$rows) {
        $hash{$r->{meal_id}}{$r->{food_category_id}} = 1;
    }
    return \%hash;
}

sub get_all_user_food_preferences {
    my $rows = $dbh->select('user_food_preferences', ['user_id', 'food_category_id'])->hashes;
    my %hash;
    for my $r (@$rows) {
        $hash{$r->{user_id}}{$r->{food_category_id}} = 1;
    }
    return \%hash;
}

sub publish_menu {
    my ($date, $meals) = @_;

    $dbh->begin_work;

    $dbh->delete('chef_selected_meals', { date => $date->ymd });

    # this could be sped up using a prepared query or bulk insert
    for my $meal_id (@$meals) {
        $dbh->insert(
            'chef_selected_meals',
            { date => $date->ymd, meal_id => $meal_id }
        );
    }

    $dbh->commit;
}

sub get_menu_meals {
    my ($date) = @_;
    my $result = $dbh->query(<<'SQL', $date);
        SELECT cm.id, cm.date, cm.meal_id, m.name
        FROM chef_selected_meals AS cm
        INNER JOIN meals AS m ON cm.meal_id = m.id
        WHERE cm.date = ?
SQL

    my $rows = $result->hashes;

    if (scalar(@$rows) == 0) {
        return undef;
    }

    my %meals;
    my @choices;
    for my $row (@$rows) {
        push(@choices, { id => $row->{id}, meal_id => $row->{meal_id} });
        $meals{$row->{meal_id}} //= { id => $row->{meal_id}, name => $row->{name} };
    }
    return { choices => \@choices, meals => \%meals };
}

sub get_orders {
    my ($date) = @_;
    my $result = $dbh->query(<<'SQL', $date);
        SELECT
            cm.id,
            m.id AS meal_id,
            m.name AS meal_name,
            COUNT(s.chef_selected_meal_id) AS order_count
        FROM chef_selected_meals AS cm
        LEFT JOIN user_selections AS s ON cm.id = s.chef_selected_meal_id
        INNER JOIN meals AS m ON cm.meal_id = m.id
        WHERE cm.date = ?
        GROUP BY cm.id
SQL

    my $rows = $result->hashes;

    my %meals;
    my @entries;
    for my $row (@$rows) {
        my $choice = { id => $row->{chef_selected_meal_id}, meal_id => $row->{meal_id} };
        push(@entries, { choice => $choice, count => $row->{order_count} });
        $meals{$row->{meal_id}} //= { id => $row->{meal_id}, name => $row->{meal_name} };
    }

    return { counts => \@entries, meals => \%meals };
}

sub set_user_selection {
    my ($user_id, $chef_selected_meal_id) = @_;

    $dbh->insert('user_selections', { user_id => $user_id, chef_selected_meal_id => $chef_selected_meal_id });
}

sub get_user_selection {
    my ($user_id, $date) = @_;

    my $result = $dbh->query(<<'SQL', $date, $user_id);
        SELECT s.chef_selected_meal_id, s.user_id, m.id AS meal_id, m.name AS meal_name
        FROM user_selections AS s
        INNER JOIN chef_selected_meals AS cm ON cm.id = s.chef_selected_meal_id
        INNER JOIN meals AS m ON cm.meal_id = m.id
        WHERE cm.date = ? AND s.user_id = ?
SQL

    my $row = $result->hash;
    return undef if not defined($row);

    my $selection = {
        chef_selected_meal_id => $row->{chef_selected_meal_id},
        user_id => $row->{user_id},
    };

    my $meal = {
        id => $row->{meal_id},
        name => $row->{meal_name},
    };

    return {
        selection => $selection,
        meal => $meal,
    };
}

sub require_logged_in {
    my ($next) = @_;

    return sub {
        my $c = shift;

        my $user_id = $c->session->{user_id};
        if (!defined($user_id)) {
            $c->app->log->debug("User not logged in, redirecting");
            $c->redirect_to('login');
            return;
        }

        my $user_info = get_user_info($user_id);
        if (!defined($user_info)) {
            $c->app->log->warn("User $user_id not found");
            delete $c->session->{user_id};
            $c->redirect_to('login');
            return;
        }

        $next->($user_info, $c, @_);
    };
}

get '/' => require_logged_in(sub {
    my ($user_info, $c) = @_;
    if ($user_info->{type} eq 'CHEF') {
        $c->redirect_to('chef-dashboard');
        return;
    }

    my $date = DateTime->today;

    my $menu_info = get_menu_meals($date);

    my $display_menu_entries = do {
        if (!defined($menu_info)) {
            undef;
        }
        else {
            my @entries = map { {
                id => $_->{id},
                name => $menu_info->{meals}{$_->{meal_id}}{name},
            } } @{$menu_info->{choices}};
            \@entries;
        }
    };

    $c->stash(menu_entries => $display_menu_entries);

    my $selection_info = get_user_selection($user_info->{id}, $date);

    $c->stash(selection_info => $selection_info);

    $c->stash(fullname => $user_info->{fullname});
    $c->render;
}) => 'index';

get '/login' => sub {
    my $c = shift;
    if (defined($c->session->{user_id})) {
        $c->app->log->info('User already logged in, redirecting');
        $c->redirect_to('index');
        return;
    }

    $c->stash->{users} = get_users;
    $c->render;
};

post '/login' => sub {
    my $c = shift;
    my $user_id = $c->param('user_id');
    die 'Missing user_id' if not defined($user_id);
    $c->app->log->debug("Logging in as user $user_id");
    $c->session->{user_id} = $user_id;
    $c->redirect_to('index');
};

post '/logout' => sub {
    my $c = shift;
    delete $c->session->{user_id};
    $c->redirect_to('login');
};

get '/food-preferences' => require_logged_in(sub {
    my ($user_info, $c) = @_;

    my %selected_categories = do {
        my $prefs = get_user_food_preferences($user_info->{id});
        map { $_->{food_category_id} => 1 } @$prefs;
    };

    my $categories = get_food_categories();

    my @preferences = map { {
            id => $_->{id},
            name => $_->{name},
            set => $selected_categories{$_->{id}}
        } } @$categories;

    $c->stash(preferences => \@preferences);

    $c->render;
});

post '/food-preferences' => require_logged_in(sub {
    my ($user_info, $c) = @_;

    my $selected_categories = $c->every_param('category[]');
    set_user_food_preferences($user_info->{id}, $selected_categories);

    $c->flash(message => 'Food preferences saved');
    $c->redirect_to('index');
});

get '/chef-dashboard' => require_logged_in(sub {
    my ($user_info, $c) = @_;
    die 'User is not a chef' if ($user_info->{type} ne 'CHEF');

    my $date = DateTime->today;

    my $display_order_entries = do {
        my $orders = get_orders($date);
        my @entries = map {
            {
                name => $orders->{meals}{$_->{choice}{meal_id}}{name},
                quantity => $_->{count},
            }
        } @{$orders->{counts}};
        \@entries;
    };

    my $screen_data;
    if (scalar(@$display_order_entries) > 0) {
        $screen_data = {
            screen => 'orders',
            orders => $display_order_entries
        };
    }
    else {
        my $users = get_users();
        my $meals = get_meals();

        my $meal_user_sets = do {
            my $all_meal_categories = get_all_meal_categories();
            my $all_user_food_preferences = get_all_user_food_preferences();
            create_meal_user_mapping(
                $meals,
                $users,
                $all_meal_categories,
                $all_user_food_preferences
            );
        };

        my $suggestions = find_max_coverage($meal_user_sets, 3);

        $screen_data = {
            screen => 'menu-editor',
            users => $users,
            meals => $meals,
            meal_user_sets => $meal_user_sets,
            suggestions => $suggestions,
        };
    }

    $c->stash(screen_data => $screen_data);

    $c->stash(fullname => $user_info->{fullname});
    $c->render;
});

sub is_menu_acceptable {
    my ($menu_meal_ids) = @_;

    my $users = get_users();
    my $meals = get_meals();

    my $meal_user_sets = do {
        my $all_meal_categories = get_all_meal_categories();
        my $all_user_food_preferences = get_all_user_food_preferences();
        create_meal_user_mapping(
            $meals,
            $users,
            $all_meal_categories,
            $all_user_food_preferences
        );
    };

    USER: for my $user (@$users) {
        for my $meal_id (@$menu_meal_ids) {
            next USER if $meal_user_sets->{$meal_id}{$user->{id}};
        }

        # If we got here, no meal was suitable for this user
        return 0;
    }

    return 1;
}

post '/chef-dashboard' => require_logged_in(sub {
    my ($user_info, $c) = @_;

    my $submitted_meals = $c->every_param('meal[]');

    my $date = DateTime->today();

    if (!is_menu_acceptable($submitted_meals)) {
        $c->flash('message' => 'Not users have a choice that satisfies their preferences');
        $c->redirect_to('chef-dashboard');
        return;
    }

    publish_menu($date, $submitted_meals);

    $c->flash('message' => 'Menu successfully published');
    $c->redirect_to('chef-dashboard');
});

post '/choose-food' => require_logged_in(sub {
    my ($user_info, $c) = @_;

    my $choice = $c->param('menu_choice');

    set_user_selection($user_info->{id}, $choice);

    $c->flash('message' => 'Food ordered');
    $c->redirect_to('index');
});

app->start;
