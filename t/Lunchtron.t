#!/usr/bin/env perl

use lib 'lib';

use Test::Spec;
use Lunchtron qw(is_subset create_meal_user_mapping);

sub to_hash {
    my ($arr) = @_;
    my %h = map { $_ => 1 } @$arr;
    return \%h;
}

describe 'to_hash' => sub {
    it 'converts an array to a hash "set"' => sub {
        is_deeply(to_hash([]), {});
        is_deeply(to_hash(['a', 'b', 'c']), {'a' => 1, 'b' => 1, 'c' => 1});
    };
};

describe 'is_subset' => sub {
    it 'returns true when a is a subset of b' => sub {
        ok(is_subset(to_hash(['a']), to_hash(['a', 'b', 'c'])));
        ok(is_subset(to_hash([]), to_hash(['a', 'b', 'c'])));
        ok(is_subset(to_hash([]), to_hash([])));
        ok(is_subset(undef, to_hash([])));
        ok(is_subset(undef, undef));
    };
    it 'returns false when a is not a subset of b' => sub {
        ok(!is_subset(to_hash(['a', 'd']), to_hash(['a', 'b', 'c'])));
    };
};

describe 'create_meal_user_mapping' => sub {
    it 'creates a mapping of meals to users who can eat them' => sub {
        my $users = [
            { id => 1, fullname => 'Adam Anderson' },
            { id => 2, fullname => 'Bob Barker' },
            { id => 3, fullname => 'Charlie Chaplin' },
        ];

        my $meals = [
            { id => 1, fullname => 'Meal A' },
            { id => 2, fullname => 'Meal B' },
            { id => 3, fullname => 'Meal C' },
        ];

        my $meal_categories = {
            1 => to_hash([1]),
            2 => to_hash([2]),
            3 => to_hash([3]),
        };

        my $user_meal_preferences = {
            1 => to_hash([1]),
            2 => to_hash([2]),
            3 => to_hash([3]),
        };

        my $expected_result = {
            1 => to_hash([1]),
            2 => to_hash([2]),
            3 => to_hash([3]),
        };

        my $actual_result = create_meal_user_mapping($meals, $users, $meal_categories, $user_meal_preferences);

        is_deeply($actual_result, $expected_result);
    };

    it 'works for a slightly more complex mapping' => sub {
        my $users = [
            { id => 1, fullname => 'Adam Anderson' },
            { id => 2, fullname => 'Bob Barker' },
            { id => 3, fullname => 'Charlie Chaplin' },
        ];

        my $meals = [
            { id => 1, fullname => 'Meal A' },
            { id => 2, fullname => 'Meal B' },
            { id => 3, fullname => 'Meal C' },
        ];

        my $meal_categories = {
            1 => to_hash([1, 2]),
            2 => to_hash([2]),
            3 => to_hash([3]),
        };

        my $user_meal_preferences = {
            1 => to_hash([1]),
            2 => to_hash([2]),
            3 => to_hash([3]),
        };

        my $expected_result = {
            1 => to_hash([1, 2]),
            2 => to_hash([2]),
            3 => to_hash([3]),
        };

        my $actual_result = create_meal_user_mapping($meals, $users, $meal_categories, $user_meal_preferences);

        is_deeply($actual_result, $expected_result);
    };
};

runtests unless caller;
